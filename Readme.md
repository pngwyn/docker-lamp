The primary purpose of this repo is to be a sample template of a LAMP setup in docker.

It is primarily geared towards Laravel, with php, composer and mssql driver set up out of the box.

The 000-default.conf also lends itself to laravel, because it is mapped the /public/ folder under the primary webroot path.
Note: Because of this, anything you put in the webroot will be not found by apache, and you will either need to change the 000-default.config or place your files in /public/

These things can be changed, as desired.
